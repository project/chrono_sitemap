<?php
/**
 * @file
 * Chrono sitemap administration interface.
 */

/**
 * Menu callback function for Chronological Sitemap settings.
 *
 * @see chrono_sitemap_menu()
 */
function chrono_sitemap_admin_settings() {
  $form = array();

  $options = array(
    '3600' => t('1 Hour'),
    '21600' => t('6 Hours'),
    '43200' => t('12 Hours'),
    '86400' => t('1 Day'),
    '604800' => t('1 Week'),
  );

  $cache_time = variable_get('chrono_sitemap_cache_time', '43200');
  $form['chrono_sitemap_cache_time'] = array(
    '#type' => 'select',
    '#title' => t('Caching time'),
    '#options' => $options,
    '#default_value' => $cache_time,
    '#description' => t('Set sitemap caching time.'),
  );

  $path = variable_get('chrono_sitemap_path', 'static/sitemap');
  $form['chrono_sitemap_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Sitemap path'),
    '#default_value' => $path,
    '#description' => t('Set sitemap path. Please, clear all caches to make this change. Follow this page !link.', array('!link' => l(t('flush cache'), 'admin/config/development/performance'))),
  );

  // Add date picker library.
  drupal_add_library('system', 'ui.datepicker');
  $listing_start = variable_get('chrono_sitemap_listing_start', '');
  $form['chrono_sitemap_listing_start'] = array(
    '#type' => 'textfield',
    '#title' => t('Start date'),
    '#default_value' => $listing_start,
    '#description' => t('Set node publish date to start showing nodes in sitemap. Defaults to oldest node publish date.'),
  );
  $form['#attached']['js']['jQuery(document).ready(function(){jQuery( "#edit-chrono-sitemap-listing-start" ).datepicker({
      dateFormat: "MM dd, yy",
      autoSize: true
    });});'] = array('type' => 'inline');

  $form['chrono_sitemap_rescan_oldest_node'] = array(
    '#type' => 'checkbox',
    '#title' => t('Rescan oldest node'),
    '#default_value' => TRUE,
    '#description' => t('Check it and submit form to get oldest published node date as "Start Date" above.'),
  );

  $form['#submit'][] = 'chrono_sitemap_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Form submit callback.
 *
 * @see chrono_sitemap_admin_settings()
 */
function chrono_sitemap_admin_settings_submit(&$form, &$form_state) {
  if (!empty($form_state['values']['chrono_sitemap_rescan_oldest_node'])) {
    chrono_sitemap_get_oldest_node();
  }
}
