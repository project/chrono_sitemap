This module ads chronological three level sitemap for nodes. All created nodes
links will be in ordered list. We can found it using path
yoursite.com/static/sitemap/%year/%month. This sitemap is for SEO purposes.
There are some settings at this page
yoursite.com http://local.cache.com/admin/config/search/chronological-sitemap.
1) This module support custom caching scheme;
2) User can change path to see the sitemap and change date from which nodes will
 be added.
3) Current month are not cached and all nodes are shown at once as they are
published.
